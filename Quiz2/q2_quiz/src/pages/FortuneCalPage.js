import Fortune  from "../components/FortuneResult";
import { useState} from "react";
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';

function FortuneCalPage(){

    const [ name,setName] = useState("");
    const [scoreResult,setScoreResult]= useState(0);
    const [translateResult, setTranslateResult]= useState("");

    const [age,setAge] = useState("");
    const [month,setMonth] = useState("");    
    
    function calculateFortune(){

        let a = parseFloat(age);
        let m = parseFloat(month);
        let cal = (a+m)/m;
        let score = parseFloat(cal);
        setScoreResult(score);
        if(scoreResult<5){
            setTranslateResult("คุณดวงดีสุดๆไปเลย");
        }else{
            setTranslateResult("คุณดวงแย่สุดๆไปเลย");
        }

    }
        return(
            <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{ marginTop : "10px"}}>
            <Grid item xs={12}>
            <Typography variant="h5">
            ยินดีต้อนรับสู่เว็บทำนายดวง  
            </Typography>
            </Grid>

            <Grid item xs={8}>
                <Box sx={{ textAlign: 'left'}}>

                คุณชื่อ : <input type="text" 
                               value={name}
                               onChange={(e)=>{setName(e.target.value);}}/> <br />
                อายุ : <input type="text" 
                               value={age}
                               onChange={(e)=>{setAge(e.target.value);}}/> <br />               
                เดือนเกิด : <input type="text" 
                               value={month}
                               onChange={(e)=>{setMonth(e.target.value);}}/> <br />
                
                <Button variant="contained" onClick={ ()=>{calculateFortune()}} >ทำนาย</Button>
                </Box>               
            </Grid>
            <Grid item xs={4}>
                {  scoreResult !=0&&
                        <div>
                            
                            นี่ผลการทำนายของคุณ
                            <Fortune
                                name={name}
                                score={scoreResult}
                                result={translateResult}
                                />
                        </div>

                }

            </Grid>
            </Grid>
            </Container>    
    );

        

}

export default FortuneCalPage