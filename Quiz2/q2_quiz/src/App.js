import logo from './logo.svg';
import './App.css';
import AboutMePage from './pages/AboutMePage';
import FortuneCalPage from './pages/FortuneCalPage';
import Header from './components/Header';
import { Routes,Route} from "react-router-dom";


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="about" element={
          <AboutMePage />
        } />

        <Route path="/" element={
          <FortuneCalPage />
        } />
          

      </Routes>
    </div>
  );
}

export default App;
